/**
 * RetanguloTeste
 */
public class RetanguloTeste {

    public static void main(String[] args) {
        double comprimento = Double.parseDouble(args[0]);
        double largura = Double.parseDouble(args[1]);

        Retangulo r = new Retangulo();

        try {
            r.setComprimento(comprimento);
            r.setLargura(largura);

            System.out.println("> Área: " + r.area());
            System.out.println("> Perímetro: " + r.perimetro());
        } catch (IllegalArgumentException e) {
            System.err.println("> Erro: Um dos lados do retângulo é igual ou menor que zero.");
        }

    }
}